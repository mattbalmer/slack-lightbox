const gulp = require('gulp');
const path = require('path');
const sync = require('gulp-sync')(gulp).sync;
let recipe = (name, config) => require('./gulp/recipes/' + name)(config);

const CONFIG = {
  NODE_MODULES: []
};

// === Env ===

gulp.task('env', recipe('env', {
  NODE_PATH: 'source/js'
}));

// === CSS bundles ===

gulp.task('css', recipe('stylus', {
  input: './source/css/index.styl',
  output: './dist/css',
  name: 'main.css'
}));

// === JS bundles ===

gulp.task('js:source', recipe('babelify', {
  input: './source/js/index.js',
  external: CONFIG.NODE_MODULES,
  output: './dist/js',
  name: 'main.js',
  transform: {
    stage: 1
  }
}));

// === Composite tasks ===

gulp.task('watch', () => {
  gulp.watch('./source/css/**/*.styl', ['css']);
  gulp.watch('./source/js/**/*.js', ['js:source']);
});

gulp.task('server', recipe('server', {
  source: './dist',
  port: process.env.PORT || 3000
}));

gulp.task('compile', ['env', 'js:source', 'css']);
gulp.task('compile-watch', ['compile', 'watch']);

gulp.task('heroku', ['compile', 'server']);

gulp.task('default', ['compile-watch', 'server']);