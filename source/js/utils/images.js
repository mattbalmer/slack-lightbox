function loadImage(src) {
  return new Promise((resolve, reject) => {
    let image = new Image();

    image.onload = function() {
      resolve(image);
    };
    
    image.src = src;
  });
}

module.exports = {
  loadImage
};