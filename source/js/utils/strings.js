/**
 * Formats a string by replacing ${KEY} with it's corresponding value in the value map.
 * eg: <i>formatString("Hello ${name}! Good to see ${you}", { name: 'World', you: 'you!' })</i> returns <i>"Hello World! Good to see you!"</i>
 * @param {string} subject The source string to perform the format on
 * @param {Object<String, *>} map The values.
 * @returns {string} the formatted string
 */
function formatString(subject, map) {
  return subject.replace(/\${(.+?)}/g, (match, key) => typeof map[key] != 'undefined' ? map[key] : '')
}

module.exports = {
  formatString
};