function scaleRect(from, to) {
  let ratioWidth = from.width / to.width;
  let ratioHeight = from.height / to.height;
  let ratio = ratioWidth > ratioHeight ? ratioWidth : ratioHeight;

  return {
    width: from.width / ratio,
    height: from.height / ratio
  };
}

module.exports = {
  scaleRect
};