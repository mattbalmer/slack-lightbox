const events = require('services/events');
const { createElement } = require('utils/dom');
const { scaleRect } = require('utils/math');
const { Templates, formatTemplate } = require('services/templates');

const Dimens = {
  MAX_WIDTH: 280,
  MAX_HEIGHT: 220
};

class GridItem {
  constructor(props) {
    this.props = props;
    this.element = null;

    this.init();
  }

  init() {
    let { width, height } = scaleRect(this.props, {
      width: Dimens.MAX_WIDTH,
      height: Dimens.MAX_HEIGHT
    });
    
    let html = formatTemplate(Templates.GRID_ITEM, {
      width,
      height,
      src: this.props.thumbnailLink
    });
    
    this.element = createElement(html);

    this.element.addEventListener('click', this.onClick.bind(this));
  }
  
  onClick() {
    events.trigger('grid-item.click', this);
  }
}

module.exports = {
  GridItem
};