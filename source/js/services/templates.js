const { formatString } = require('utils/strings');

function loadTemplate(id) {
  if(TemplateCache[id]) {
    return TemplateCache[id];
  } else {
    let el = document.getElementById(id);
    return TemplateCache[id] = (el ? el.innerHTML : null);
  }
}

function formatTemplate(templateId, variables) {
  let template = loadTemplate(templateId);
  return formatString(template, variables);
}

const TemplateCache = {};
const Templates = {
  GRID_ITEM: 'grid-item',
  LIGHTBOX_CONTENT: 'lightbox-content'
};

module.exports = {
  Templates,
  formatTemplate
};