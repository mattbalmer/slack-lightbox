const { EventHub } = require('utils/event-hub');

const lightboxEventHub = new EventHub('slack.lightbox');

module.exports = lightboxEventHub;