const http = require('utils/http');

const URL = 'https://www.googleapis.com/customsearch/v1';
const ENGINE_ID = '003337539191910196419:g3uzpzgshfy';
const API_KEY = 'AIzaSyANslNnCsuch-iTFNkg5xLoTKY2-_8aPtg';

function search(query) {
  return http.get(URL, {
    q: query,
    cx: ENGINE_ID,
    key: API_KEY,
    start: 1,
    searchType: 'image'
  })
    .then(res => JSON.parse(res.body))
}

module.exports = {
  search
};