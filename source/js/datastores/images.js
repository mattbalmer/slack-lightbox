const images = {
  _images: null,

  set(images) {
    return this._images = images;
  },

  get() {
    return this._images;
  }
};

module.exports = images;