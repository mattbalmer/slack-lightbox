const { GridItem } = require('components/grid-item');

const Selectors = {
  GRID_DIRECTIVE: '[data-d-grid]'
};

class Grid {
  constructor(element, props) {
    this.element = element;
    this.props = props;

    this.init();
  }

  init() {
    this.items = this.props.items.map(item => new GridItem(item));
    this.items.forEach(item => this.element.appendChild(item.element));
  }
}

module.exports = {
  Grid,
  GridSelectors: Selectors
};