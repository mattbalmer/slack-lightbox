const api = require('services/image-api');
const http = require('utils/http');
const events = require('services/events');
const images = require('datastores/images');
const { transformImages } = require('transformers/images');
const { Grid } = require('directives/grid');
const { Lightbox } = require('directives/lightbox');

const Selectors = {
  APP_DIRECTIVE: '[data-d-app]',
  GRID_DIRECTIVE: '[data-d-grid]',
  LIGHTBOX_DIRECTIVE: '[data-d-lightbox]'
};

class App {
  constructor(element, props) {
    this.element = element;
    this.props = props;

    this.grid = null;
    this.lightbox = null;

    this.init();
  }

  init() {
    api.search(this.props.query)
      .then(data => this.render(data))
      .catch(res => {
        // limit exceeded
        if(res.status == 403) {
          http.get('/sample.json')
            .then(res => this.render(JSON.parse(res.body)))
        }
      })
    ;
  }

  render(data) {
    let items = transformImages(data.items);

    images.set(items);

    this.grid = new Grid(document.querySelector(Selectors.GRID_DIRECTIVE), { items });
    this.lightbox = new Lightbox(document.querySelector(Selectors.LIGHTBOX_DIRECTIVE), {});

    events.on('grid-item.click', gridItem => {
      this.lightbox.show(gridItem.props);
    });
  }

  static mount(query) {
    return new App(document.querySelector(Selectors.APP_DIRECTIVE), { query });
  }
}

module.exports = {
  App
};