const images = require('datastores/images');
const { Templates, formatTemplate } = require('services/templates');
const { createElement } = require('utils/dom');
const { scaleRect } = require('utils/math');
const { loadImage } = require('utils/images');

const Classes = {
  IS_VISIBLE: 'is-visible',
  IS_HIDDEN: 'is-hidden',
  IS_SCROLL_LOCKED: 'is-scroll-locked',
  IS_LOADED: 'is-loaded'
};

const Selectors = {
  LIGHTBOX_DIRECTIVE: '[data-d-lightbox]'
};

const Dimens = {
  MAX_WIDTH: 1280,
  MAX_HEIGHT: 780
};

class Lightbox {
  constructor(element, props) {
    this.element = element;
    this.props = props;

    this.current = null;
    this.shadow = this.element.querySelector('[data-d-lightbox-shadow]');
    this.content = this.element.querySelector('[data-d-lightbox-content]');
    this.title = this.element.querySelector('[data-d-lightbox-title]');
    this.close = this.element.querySelector('[data-d-lightbox-close]');
    this.next = this.element.querySelector('[data-d-lightbox-arrow="next"]');
    this.previous = this.element.querySelector('[data-d-lightbox-arrow="previous"]');

    this.init();
  }

  init() {
    this.shadow.addEventListener('click', this.hide.bind(this));
    this.close.addEventListener('click', this.hide.bind(this));

    this.next.addEventListener('click', this.change.bind(this, 1));
    this.previous.addEventListener('click', this.change.bind(this, -1));

    window.addEventListener('resize', () => {
      if(this.element.classList.contains(Classes.IS_VISIBLE) && this.current) {
        this.fillContent(this.current);
      }
    });
  }
  
  change(direction) {
    let items = images.get();
    let nextIndex = this.current.index + direction;
    if(nextIndex >= items.length) nextIndex = 0;
    if(nextIndex < 0) nextIndex = items.length - 1;
    this.show(items[nextIndex]);
  }

  show(props) {
    this.element.classList.add(Classes.IS_VISIBLE);
    this.element.classList.remove(Classes.IS_HIDDEN);
    this.element.classList.remove(Classes.IS_LOADED);

    document.documentElement.classList.add(Classes.IS_SCROLL_LOCKED);

    this.fillContent(props);
  }

  hide() {
    this.element.classList.remove(Classes.IS_VISIBLE);
    this.element.classList.add(Classes.IS_HIDDEN);
    this.element.classList.remove(Classes.IS_LOADED);

    document.documentElement.classList.remove(Classes.IS_SCROLL_LOCKED);

    this.resetContent();
  }

  resetContent() {
    this.content.style.width = '';
    this.content.style.height = '';
    setTimeout(() => {
      this.content.innerHTML = '';
    }, 400);
  }

  fillContent(image) {
    this.current = image;

    let { width, height } = scaleRect(image, {
      width: Math.min(Dimens.MAX_WIDTH, window.innerWidth - 40),
      height: Math.min(Dimens.MAX_HEIGHT, window.innerHeight - 40)
    });

    loadImage(image.link)
      .then(img => {
        this.element.classList.add(Classes.IS_LOADED);

        let html = formatTemplate(Templates.LIGHTBOX_CONTENT, {
          width,
          height,
          src: image.link
        });

        this.title.innerText = image.title;
        this.content.style.width = `${width}px`;
        this.content.style.height = `${height}px`;
        this.content.innerHTML = '';
        this.content.appendChild(createElement(html));
      })
  }
}

module.exports = {
  Lightbox,
  LightboxSelectors: Selectors
};