function transform(raw) {
  return raw.map((item, index) => {
    return {
      index,
      title: item.title,
      width: item.image.width,
      height: item.image.height,
      thumbnailLink: item.image.thumbnailLink,
      link: item.link
    }
  })
}

module.exports = {
  transformImages: transform
};