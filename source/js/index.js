const { App } = require('directives/app');

let query = location.search ? (/\?q=([^&]*)/g).exec(location.search)[1] : null;

App.mount(query || 'bacon');