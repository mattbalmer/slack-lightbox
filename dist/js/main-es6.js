(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
const events = require('services/events');
const { createElement } = require('utils/dom');
const { scaleRect } = require('utils/math');
const { Templates, formatTemplate } = require('services/templates');

const Dimens = {
  MAX_WIDTH: 280,
  MAX_HEIGHT: 220
};

class GridItem {
  constructor(props) {
    this.props = props;
    this.element = null;

    this.init();
  }

  init() {
    let { width, height } = scaleRect(this.props, {
      width: Dimens.MAX_WIDTH,
      height: Dimens.MAX_HEIGHT
    });
    
    let html = formatTemplate(Templates.GRID_ITEM, {
      width,
      height,
      src: this.props.thumbnailLink
    });
    
    this.element = createElement(html);

    this.element.addEventListener('click', this.onClick.bind(this));
  }
  
  onClick() {
    events.trigger('grid-item.click', this);
  }
}

module.exports = {
  GridItem
};
},{"services/events":7,"services/templates":9,"utils/dom":11,"utils/math":15}],2:[function(require,module,exports){
const images = {
  _images: null,

  set(images) {
    return this._images = images;
  },

  get() {
    return this._images;
  }
};

module.exports = images;
},{}],3:[function(require,module,exports){
const api = require('services/image-api');
const http = require('utils/http');
const events = require('services/events');
const images = require('datastores/images');
const { transformImages } = require('transformers/images');
const { Grid } = require('directives/grid');
const { Lightbox } = require('directives/lightbox');

const Selectors = {
  APP_DIRECTIVE: '[data-d-app]',
  GRID_DIRECTIVE: '[data-d-grid]',
  LIGHTBOX_DIRECTIVE: '[data-d-lightbox]'
};

class App {
  constructor(element, props) {
    this.element = element;
    this.props = props;

    this.grid = null;
    this.lightbox = null;

    this.init();
  }

  init() {
    api.search(this.props.query)
      .then(data => this.render(data))
      .catch(res => {
        // limit exceeded
        if(res.status == 403) {
          http.get('/sample.json')
            .then(res => this.render(JSON.parse(res.body)))
        }
      })
    ;
  }

  render(data) {
    let items = transformImages(data.items);

    images.set(items);

    this.grid = new Grid(document.querySelector(Selectors.GRID_DIRECTIVE), { items });
    this.lightbox = new Lightbox(document.querySelector(Selectors.LIGHTBOX_DIRECTIVE), {});

    events.on('grid-item.click', gridItem => {
      this.lightbox.show(gridItem.props);
    });
  }

  static mount(query) {
    return new App(document.querySelector(Selectors.APP_DIRECTIVE), { query });
  }
}

module.exports = {
  App
};
},{"datastores/images":2,"directives/grid":4,"directives/lightbox":5,"services/events":7,"services/image-api":8,"transformers/images":10,"utils/http":13}],4:[function(require,module,exports){
const { GridItem } = require('components/grid-item');

const Selectors = {
  GRID_DIRECTIVE: '[data-d-grid]'
};

class Grid {
  constructor(element, props) {
    this.element = element;
    this.props = props;

    this.init();
  }

  init() {
    this.items = this.props.items.map(item => new GridItem(item));
    this.items.forEach(item => this.element.appendChild(item.element));
  }
}

module.exports = {
  Grid,
  GridSelectors: Selectors
};
},{"components/grid-item":1}],5:[function(require,module,exports){
const images = require('datastores/images');
const { Templates, formatTemplate } = require('services/templates');
const { createElement } = require('utils/dom');
const { scaleRect } = require('utils/math');
const { loadImage } = require('utils/images');

const Classes = {
  IS_VISIBLE: 'is-visible',
  IS_HIDDEN: 'is-hidden',
  IS_SCROLL_LOCKED: 'is-scroll-locked',
  IS_LOADED: 'is-loaded'
};

const Selectors = {
  LIGHTBOX_DIRECTIVE: '[data-d-lightbox]'
};

const Dimens = {
  MAX_WIDTH: 1280,
  MAX_HEIGHT: 780
};

class Lightbox {
  constructor(element, props) {
    this.element = element;
    this.props = props;

    this.current = null;
    this.shadow = this.element.querySelector('[data-d-lightbox-shadow]');
    this.content = this.element.querySelector('[data-d-lightbox-content]');
    this.close = this.element.querySelector('[data-d-lightbox-close]');
    this.next = this.element.querySelector('[data-d-lightbox-arrow="next"]');
    this.previous = this.element.querySelector('[data-d-lightbox-arrow="previous"]');

    this.init();
  }

  init() {
    this.shadow.addEventListener('click', this.hide.bind(this));
    this.close.addEventListener('click', this.hide.bind(this));

    this.next.addEventListener('click', this.change.bind(this, 1));
    this.previous.addEventListener('click', this.change.bind(this, -1));

    window.addEventListener('resize', () => {
      if(this.element.classList.contains(Classes.IS_VISIBLE) && this.current) {
        this.fillContent(this.current);
      }
    });
  }
  
  change(direction) {
    let items = images.get();
    let nextIndex = this.current.index + direction;
    if(nextIndex >= items.length) nextIndex = 0;
    if(nextIndex < 0) nextIndex = items.length - 1;
    this.show(items[nextIndex]);
  }

  show(props) {
    this.element.classList.add(Classes.IS_VISIBLE);
    this.element.classList.remove(Classes.IS_HIDDEN);
    this.element.classList.remove(Classes.IS_LOADED);

    document.documentElement.classList.add(Classes.IS_SCROLL_LOCKED);

    this.fillContent(props);
  }

  hide() {
    this.element.classList.remove(Classes.IS_VISIBLE);
    this.element.classList.add(Classes.IS_HIDDEN);
    this.element.classList.remove(Classes.IS_LOADED);

    document.documentElement.classList.remove(Classes.IS_SCROLL_LOCKED);

    this.resetContent();
  }

  resetContent() {
    this.content.style.width = '';
    this.content.style.height = '';
    setTimeout(() => {
      this.content.innerHTML = '';
    }, 400);
  }

  fillContent(image) {
    this.current = image;

    let { width, height } = scaleRect(image, {
      width: Math.min(Dimens.MAX_WIDTH, window.innerWidth - 40),
      height: Math.min(Dimens.MAX_HEIGHT, window.innerHeight - 40)
    });

    loadImage(image.link)
      .then(img => {
        this.element.classList.add(Classes.IS_LOADED);

        let html = formatTemplate(Templates.LIGHTBOX_CONTENT, {
          width,
          height,
          src: image.link
        });

        this.content.style.width = `${width}px`;
        this.content.style.height = `${height}px`;
        this.content.innerHTML = '';
        this.content.appendChild(createElement(html));
      })
  }
}

module.exports = {
  Lightbox,
  LightboxSelectors: Selectors
};
},{"datastores/images":2,"services/templates":9,"utils/dom":11,"utils/images":14,"utils/math":15}],6:[function(require,module,exports){
const { App } = require('directives/app');

let query = location.search ? (/\?q=([^&]*)/g).exec(location.search)[1] : null;

App.mount(query || 'bacon');
},{"directives/app":3}],7:[function(require,module,exports){
const { EventHub } = require('utils/event-hub');

const lightboxEventHub = new EventHub('slack.lightbox');

module.exports = lightboxEventHub;
},{"utils/event-hub":12}],8:[function(require,module,exports){
const http = require('utils/http');

const URL = 'https://www.googleapis.com/customsearch/v1';
const ENGINE_ID = '003337539191910196419:g3uzpzgshfy';
const API_KEY = 'AIzaSyANslNnCsuch-iTFNkg5xLoTKY2-_8aPtg';

function search(query) {
  return http.get(URL, {
    q: query,
    cx: ENGINE_ID,
    key: API_KEY,
    start: 1,
    searchType: 'image'
  })
    .then(res => JSON.parse(res.body))
}

module.exports = {
  search
};
},{"utils/http":13}],9:[function(require,module,exports){
const { formatString } = require('utils/strings');

function loadTemplate(id) {
  if(TemplateCache[id]) {
    return TemplateCache[id];
  } else {
    let el = document.getElementById(id);
    return TemplateCache[id] = (el ? el.innerHTML : null);
  }
}

function formatTemplate(templateId, variables) {
  let template = loadTemplate(templateId);
  return formatString(template, variables);
}

const TemplateCache = {};
const Templates = {
  GRID_ITEM: 'grid-item',
  LIGHTBOX_CONTENT: 'lightbox-content'
};

module.exports = {
  Templates,
  formatTemplate
};
},{"utils/strings":16}],10:[function(require,module,exports){
function transform(raw) {
  return raw.map((item, index) => {
    return {
      index,
      width: item.image.width,
      height: item.image.height,
      thumbnailLink: item.image.thumbnailLink,
      link: item.link
    }
  })
}

module.exports = {
  transformImages: transform
};
},{}],11:[function(require,module,exports){
/**
 * Creates a DOM element from an HTML string.
 *
 * @param {string} html The HTML string to render.
 * @returns {Element} The DOM element constructed.
 */
function createElement(html) {
    let wrapper = document.createElement('div');
    wrapper.innerHTML = html;
    return wrapper.firstElementChild;
}

module.exports = {
    createElement
};
},{}],12:[function(require,module,exports){
class Subscriber {
    constructor(id, event, callback) {
        this.id = id;
        this.event = event;
        this.callback = callback;
    }

    trigger(...args) {
        this.callback(...args);
    }
}

class EventHub {
    constructor(id = null) {
        this.id = id;

        this.subscribers_ = [];

        this.nextId_ = 1;
    }

    nextSubscriberID_() {
        return this.nextId_++;
    }

    on(event, callback) {
        let subscriber = new Subscriber(this.nextSubscriberID_(), event, callback);
        subscriber.destroy = this.destroy.bind(this, subscriber);

        this.subscribers_.push(subscriber);

        return subscriber;
    }

    trigger(event, ...args) {
        this.subscribers_
            .filter(s => s.event == event)
            .forEach(s => s.trigger(...args));
    }

    destroy(subscriber) {
        this.subscribers_ = this.subscribers_
            .filter(s => s.id != subscriber.id);
    }
}

module.exports = {
    EventHub
};
},{}],13:[function(require,module,exports){
/**
 * Execute an AJAX request.
 *
 * @param {string} method The HTTP method to use.
 * @param {string} url The URL to send teh request to.
 * @param {object=} params Key-value paris of query string parameters.
 * @param {*=} data The request body.
 * @returns {Promise}
 */
function http({ method, url, params, data }) {
    return new Promise((resolve, reject) => {
        let client = new XMLHttpRequest();
        let uri = url;

        if(params) {
            let queryString = '';
            for(let key in params) {
                if(!params.hasOwnProperty(key)) continue;
                let symbol = queryString ? '&' : '?';
                let s = `${symbol}${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`;
                queryString += s;
            }
            uri += queryString;
        }

        client.open(method, uri);

        if(data && (method == 'POST' || method == 'PUT')) {
            client.send(data);
        } else {
            client.send();
        }

        client.onload = function() {
            if(this.status >= 200 && this.status < 300) {
                resolve({
                    body: this.response,
                    status: this.status
                });
            } else {
                reject({
                    body: this.statusText,
                    status: this.status
                });
            }
        };

        client.onerror = function() {
            reject({
                body: this.statusText,
                status: this.status
            })
        };
    })
}

/**
 * Executes a GET request to the specified location.
 *
 * @param {string} url The URL to send the request to.
 * @param {object=} params Key-value pairs of query string parameters.
 * @returns {Promise}
 */
http['get'] = (url, params) => http({
    url,
    params,
    method: 'GET'
});

module.exports = http;
},{}],14:[function(require,module,exports){
function loadImage(src) {
  return new Promise((resolve, reject) => {
    let image = new Image();

    image.onload = function() {
      resolve(image);
    };
    
    image.src = src;
  });
}

module.exports = {
  loadImage
};
},{}],15:[function(require,module,exports){
function scaleRect(from, to) {
  let ratioWidth = from.width / to.width;
  let ratioHeight = from.height / to.height;
  let ratio = ratioWidth > ratioHeight ? ratioWidth : ratioHeight;

  return {
    width: from.width / ratio,
    height: from.height / ratio
  };
}

module.exports = {
  scaleRect
};
},{}],16:[function(require,module,exports){
/**
 * Formats a string by replacing ${KEY} with it's corresponding value in the value map.
 * eg: <i>formatString("Hello ${name}! Good to see ${you}", { name: 'World', you: 'you!' })</i> returns <i>"Hello World! Good to see you!"</i>
 * @param {string} subject The source string to perform the format on
 * @param {Object<String, *>} map The values.
 * @returns {string} the formatted string
 */
function formatString(subject, map) {
  return subject.replace(/\${(.+?)}/g, (match, key) => typeof map[key] != 'undefined' ? map[key] : '')
}

module.exports = {
  formatString
};
},{}]},{},[6]);
