(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var events = require('services/events');

var _require = require('utils/dom');

var createElement = _require.createElement;

var _require2 = require('utils/math');

var scaleRect = _require2.scaleRect;

var _require3 = require('services/templates');

var Templates = _require3.Templates;
var formatTemplate = _require3.formatTemplate;

var Dimens = {
  MAX_WIDTH: 280,
  MAX_HEIGHT: 220
};

var GridItem = (function () {
  function GridItem(props) {
    _classCallCheck(this, GridItem);

    this.props = props;
    this.element = null;

    this.init();
  }

  _createClass(GridItem, [{
    key: 'init',
    value: function init() {
      var _scaleRect = scaleRect(this.props, {
        width: Dimens.MAX_WIDTH,
        height: Dimens.MAX_HEIGHT
      });

      var width = _scaleRect.width;
      var height = _scaleRect.height;

      var html = formatTemplate(Templates.GRID_ITEM, {
        width: width,
        height: height,
        src: this.props.thumbnailLink
      });

      this.element = createElement(html);

      this.element.addEventListener('click', this.onClick.bind(this));
    }
  }, {
    key: 'onClick',
    value: function onClick() {
      events.trigger('grid-item.click', this);
    }
  }]);

  return GridItem;
})();

module.exports = {
  GridItem: GridItem
};

},{"services/events":7,"services/templates":9,"utils/dom":11,"utils/math":15}],2:[function(require,module,exports){
"use strict";

var images = {
  _images: null,

  set: function set(images) {
    return this._images = images;
  },

  get: function get() {
    return this._images;
  }
};

module.exports = images;

},{}],3:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var api = require('services/image-api');
var http = require('utils/http');
var events = require('services/events');
var images = require('datastores/images');

var _require = require('transformers/images');

var transformImages = _require.transformImages;

var _require2 = require('directives/grid');

var Grid = _require2.Grid;

var _require3 = require('directives/lightbox');

var Lightbox = _require3.Lightbox;

var Selectors = {
  APP_DIRECTIVE: '[data-d-app]',
  GRID_DIRECTIVE: '[data-d-grid]',
  LIGHTBOX_DIRECTIVE: '[data-d-lightbox]'
};

var App = (function () {
  function App(element, props) {
    _classCallCheck(this, App);

    this.element = element;
    this.props = props;

    this.grid = null;
    this.lightbox = null;

    this.init();
  }

  _createClass(App, [{
    key: 'init',
    value: function init() {
      var _this = this;

      api.search(this.props.query).then(function (data) {
        return _this.render(data);
      })['catch'](function (res) {
        // limit exceeded
        if (res.status == 403) {
          http.get('/sample.json').then(function (res) {
            return _this.render(JSON.parse(res.body));
          });
        }
      });
    }
  }, {
    key: 'render',
    value: function render(data) {
      var _this2 = this;

      var items = transformImages(data.items);

      images.set(items);

      this.grid = new Grid(document.querySelector(Selectors.GRID_DIRECTIVE), { items: items });
      this.lightbox = new Lightbox(document.querySelector(Selectors.LIGHTBOX_DIRECTIVE), {});

      events.on('grid-item.click', function (gridItem) {
        _this2.lightbox.show(gridItem.props);
      });
    }
  }], [{
    key: 'mount',
    value: function mount(query) {
      return new App(document.querySelector(Selectors.APP_DIRECTIVE), { query: query });
    }
  }]);

  return App;
})();

module.exports = {
  App: App
};

},{"datastores/images":2,"directives/grid":4,"directives/lightbox":5,"services/events":7,"services/image-api":8,"transformers/images":10,"utils/http":13}],4:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _require = require('components/grid-item');

var GridItem = _require.GridItem;

var Selectors = {
  GRID_DIRECTIVE: '[data-d-grid]'
};

var Grid = (function () {
  function Grid(element, props) {
    _classCallCheck(this, Grid);

    this.element = element;
    this.props = props;

    this.init();
  }

  _createClass(Grid, [{
    key: 'init',
    value: function init() {
      var _this = this;

      this.items = this.props.items.map(function (item) {
        return new GridItem(item);
      });
      this.items.forEach(function (item) {
        return _this.element.appendChild(item.element);
      });
    }
  }]);

  return Grid;
})();

module.exports = {
  Grid: Grid,
  GridSelectors: Selectors
};

},{"components/grid-item":1}],5:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var images = require('datastores/images');

var _require = require('services/templates');

var Templates = _require.Templates;
var formatTemplate = _require.formatTemplate;

var _require2 = require('utils/dom');

var createElement = _require2.createElement;

var _require3 = require('utils/math');

var scaleRect = _require3.scaleRect;

var _require4 = require('utils/images');

var loadImage = _require4.loadImage;

var Classes = {
  IS_VISIBLE: 'is-visible',
  IS_HIDDEN: 'is-hidden',
  IS_SCROLL_LOCKED: 'is-scroll-locked',
  IS_LOADED: 'is-loaded'
};

var Selectors = {
  LIGHTBOX_DIRECTIVE: '[data-d-lightbox]'
};

var Dimens = {
  MAX_WIDTH: 1280,
  MAX_HEIGHT: 780
};

var Lightbox = (function () {
  function Lightbox(element, props) {
    _classCallCheck(this, Lightbox);

    this.element = element;
    this.props = props;

    this.current = null;
    this.shadow = this.element.querySelector('[data-d-lightbox-shadow]');
    this.content = this.element.querySelector('[data-d-lightbox-content]');
    this.title = this.element.querySelector('[data-d-lightbox-title]');
    this.close = this.element.querySelector('[data-d-lightbox-close]');
    this.next = this.element.querySelector('[data-d-lightbox-arrow="next"]');
    this.previous = this.element.querySelector('[data-d-lightbox-arrow="previous"]');

    this.init();
  }

  _createClass(Lightbox, [{
    key: 'init',
    value: function init() {
      var _this = this;

      this.shadow.addEventListener('click', this.hide.bind(this));
      this.close.addEventListener('click', this.hide.bind(this));

      this.next.addEventListener('click', this.change.bind(this, 1));
      this.previous.addEventListener('click', this.change.bind(this, -1));

      window.addEventListener('resize', function () {
        if (_this.element.classList.contains(Classes.IS_VISIBLE) && _this.current) {
          _this.fillContent(_this.current);
        }
      });
    }
  }, {
    key: 'change',
    value: function change(direction) {
      var items = images.get();
      var nextIndex = this.current.index + direction;
      if (nextIndex >= items.length) nextIndex = 0;
      if (nextIndex < 0) nextIndex = items.length - 1;
      this.show(items[nextIndex]);
    }
  }, {
    key: 'show',
    value: function show(props) {
      this.element.classList.add(Classes.IS_VISIBLE);
      this.element.classList.remove(Classes.IS_HIDDEN);
      this.element.classList.remove(Classes.IS_LOADED);

      document.documentElement.classList.add(Classes.IS_SCROLL_LOCKED);

      this.fillContent(props);
    }
  }, {
    key: 'hide',
    value: function hide() {
      this.element.classList.remove(Classes.IS_VISIBLE);
      this.element.classList.add(Classes.IS_HIDDEN);
      this.element.classList.remove(Classes.IS_LOADED);

      document.documentElement.classList.remove(Classes.IS_SCROLL_LOCKED);

      this.resetContent();
    }
  }, {
    key: 'resetContent',
    value: function resetContent() {
      var _this2 = this;

      this.content.style.width = '';
      this.content.style.height = '';
      setTimeout(function () {
        _this2.content.innerHTML = '';
      }, 400);
    }
  }, {
    key: 'fillContent',
    value: function fillContent(image) {
      var _this3 = this;

      this.current = image;

      var _scaleRect = scaleRect(image, {
        width: Math.min(Dimens.MAX_WIDTH, window.innerWidth - 40),
        height: Math.min(Dimens.MAX_HEIGHT, window.innerHeight - 40)
      });

      var width = _scaleRect.width;
      var height = _scaleRect.height;

      loadImage(image.link).then(function (img) {
        _this3.element.classList.add(Classes.IS_LOADED);

        var html = formatTemplate(Templates.LIGHTBOX_CONTENT, {
          width: width,
          height: height,
          src: image.link
        });

        _this3.title.innerText = image.title;
        _this3.content.style.width = width + 'px';
        _this3.content.style.height = height + 'px';
        _this3.content.innerHTML = '';
        _this3.content.appendChild(createElement(html));
      });
    }
  }]);

  return Lightbox;
})();

module.exports = {
  Lightbox: Lightbox,
  LightboxSelectors: Selectors
};

},{"datastores/images":2,"services/templates":9,"utils/dom":11,"utils/images":14,"utils/math":15}],6:[function(require,module,exports){
'use strict';

var _require = require('directives/app');

var App = _require.App;

var query = location.search ? /\?q=([^&]*)/g.exec(location.search)[1] : null;

App.mount(query || 'bacon');

},{"directives/app":3}],7:[function(require,module,exports){
'use strict';

var _require = require('utils/event-hub');

var EventHub = _require.EventHub;

var lightboxEventHub = new EventHub('slack.lightbox');

module.exports = lightboxEventHub;

},{"utils/event-hub":12}],8:[function(require,module,exports){
'use strict';

var http = require('utils/http');

var URL = 'https://www.googleapis.com/customsearch/v1';
var ENGINE_ID = '003337539191910196419:g3uzpzgshfy';
var API_KEY = 'AIzaSyANslNnCsuch-iTFNkg5xLoTKY2-_8aPtg';

function search(query) {
  return http.get(URL, {
    q: query,
    cx: ENGINE_ID,
    key: API_KEY,
    start: 1,
    searchType: 'image'
  }).then(function (res) {
    return JSON.parse(res.body);
  });
}

module.exports = {
  search: search
};

},{"utils/http":13}],9:[function(require,module,exports){
'use strict';

var _require = require('utils/strings');

var formatString = _require.formatString;

function loadTemplate(id) {
  if (TemplateCache[id]) {
    return TemplateCache[id];
  } else {
    var el = document.getElementById(id);
    return TemplateCache[id] = el ? el.innerHTML : null;
  }
}

function formatTemplate(templateId, variables) {
  var template = loadTemplate(templateId);
  return formatString(template, variables);
}

var TemplateCache = {};
var Templates = {
  GRID_ITEM: 'grid-item',
  LIGHTBOX_CONTENT: 'lightbox-content'
};

module.exports = {
  Templates: Templates,
  formatTemplate: formatTemplate
};

},{"utils/strings":16}],10:[function(require,module,exports){
"use strict";

function transform(raw) {
  return raw.map(function (item, index) {
    return {
      index: index,
      title: item.title,
      width: item.image.width,
      height: item.image.height,
      thumbnailLink: item.image.thumbnailLink,
      link: item.link
    };
  });
}

module.exports = {
  transformImages: transform
};

},{}],11:[function(require,module,exports){
/**
 * Creates a DOM element from an HTML string.
 *
 * @param {string} html The HTML string to render.
 * @returns {Element} The DOM element constructed.
 */
'use strict';

function createElement(html) {
    var wrapper = document.createElement('div');
    wrapper.innerHTML = html;
    return wrapper.firstElementChild;
}

module.exports = {
    createElement: createElement
};

},{}],12:[function(require,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Subscriber = (function () {
    function Subscriber(id, event, callback) {
        _classCallCheck(this, Subscriber);

        this.id = id;
        this.event = event;
        this.callback = callback;
    }

    _createClass(Subscriber, [{
        key: "trigger",
        value: function trigger() {
            this.callback.apply(this, arguments);
        }
    }]);

    return Subscriber;
})();

var EventHub = (function () {
    function EventHub() {
        var id = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];

        _classCallCheck(this, EventHub);

        this.id = id;

        this.subscribers_ = [];

        this.nextId_ = 1;
    }

    _createClass(EventHub, [{
        key: "nextSubscriberID_",
        value: function nextSubscriberID_() {
            return this.nextId_++;
        }
    }, {
        key: "on",
        value: function on(event, callback) {
            var subscriber = new Subscriber(this.nextSubscriberID_(), event, callback);
            subscriber.destroy = this.destroy.bind(this, subscriber);

            this.subscribers_.push(subscriber);

            return subscriber;
        }
    }, {
        key: "trigger",
        value: function trigger(event) {
            for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                args[_key - 1] = arguments[_key];
            }

            this.subscribers_.filter(function (s) {
                return s.event == event;
            }).forEach(function (s) {
                return s.trigger.apply(s, args);
            });
        }
    }, {
        key: "destroy",
        value: function destroy(subscriber) {
            this.subscribers_ = this.subscribers_.filter(function (s) {
                return s.id != subscriber.id;
            });
        }
    }]);

    return EventHub;
})();

module.exports = {
    EventHub: EventHub
};

},{}],13:[function(require,module,exports){
/**
 * Execute an AJAX request.
 *
 * @param {string} method The HTTP method to use.
 * @param {string} url The URL to send teh request to.
 * @param {object=} params Key-value paris of query string parameters.
 * @param {*=} data The request body.
 * @returns {Promise}
 */
'use strict';

function http(_ref) {
    var method = _ref.method;
    var url = _ref.url;
    var params = _ref.params;
    var data = _ref.data;

    return new Promise(function (resolve, reject) {
        var client = new XMLHttpRequest();
        var uri = url;

        if (params) {
            var queryString = '';
            for (var key in params) {
                if (!params.hasOwnProperty(key)) continue;
                var symbol = queryString ? '&' : '?';
                var s = '' + symbol + encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
                queryString += s;
            }
            uri += queryString;
        }

        client.open(method, uri);

        if (data && (method == 'POST' || method == 'PUT')) {
            client.send(data);
        } else {
            client.send();
        }

        client.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve({
                    body: this.response,
                    status: this.status
                });
            } else {
                reject({
                    body: this.statusText,
                    status: this.status
                });
            }
        };

        client.onerror = function () {
            reject({
                body: this.statusText,
                status: this.status
            });
        };
    });
}

/**
 * Executes a GET request to the specified location.
 *
 * @param {string} url The URL to send the request to.
 * @param {object=} params Key-value pairs of query string parameters.
 * @returns {Promise}
 */
http['get'] = function (url, params) {
    return http({
        url: url,
        params: params,
        method: 'GET'
    });
};

module.exports = http;

},{}],14:[function(require,module,exports){
"use strict";

function loadImage(src) {
  return new Promise(function (resolve, reject) {
    var image = new Image();

    image.onload = function () {
      resolve(image);
    };

    image.src = src;
  });
}

module.exports = {
  loadImage: loadImage
};

},{}],15:[function(require,module,exports){
"use strict";

function scaleRect(from, to) {
  var ratioWidth = from.width / to.width;
  var ratioHeight = from.height / to.height;
  var ratio = ratioWidth > ratioHeight ? ratioWidth : ratioHeight;

  return {
    width: from.width / ratio,
    height: from.height / ratio
  };
}

module.exports = {
  scaleRect: scaleRect
};

},{}],16:[function(require,module,exports){
/**
 * Formats a string by replacing ${KEY} with it's corresponding value in the value map.
 * eg: <i>formatString("Hello ${name}! Good to see ${you}", { name: 'World', you: 'you!' })</i> returns <i>"Hello World! Good to see you!"</i>
 * @param {string} subject The source string to perform the format on
 * @param {Object<String, *>} map The values.
 * @returns {string} the formatted string
 */
'use strict';

function formatString(subject, map) {
  return subject.replace(/\${(.+?)}/g, function (match, key) {
    return typeof map[key] != 'undefined' ? map[key] : '';
  });
}

module.exports = {
  formatString: formatString
};

},{}]},{},[6]);
