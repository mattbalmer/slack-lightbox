# Slack Lightbox (Interview Assignment)

## Install

Note: *requires node version 6.0 or greater*

    $ npm install
    $ gulp

Open `http://localhost:3000`

## Usage

Control the search query with the `q` query string param. Eg. `http://localhost:3000?q=avocados` will return images of avocados. Defaults to bacon.

Will default to using `sample.json` if the daily query limit (100) is exceeded. The sample JSON contains apples.

## Code Notes

The CSS is fairly straightforward. Broken down into base-level changes in the main directory, section-specific CSS in `sections` (unique parts of the page), and component-specific CSS in `components` (reusable and potentially duplicated usages).

The JS is a bit more separated.

`components` are modules that render their own templates, and need to be attached to the DOM.
`directives` are modules that attach to DOM elements, and use the markup provided.
`datastores` are dumb objects for storing/retrieving data throughout the app.
`services` are configurable or state-driven pieces of code tied to one specific domain (eg. an API, or template-rendering)
`transformers` take raw data input and spit out only the data that's needed for the app, in a more usable format.
`utils` are domain-specific conglomerations of functions.